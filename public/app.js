let app = new Vue({
    el: '#app',
    created() {
        this.fetchData();
    },
    data: {
        cards: [],
        categories: [
            {
                name: 'good',
                title: 'What went well?',
                isAdding: false,
                newContent: null
            },
            {
                name: 'bad',
                title: 'What could be improved?',
                isAdding: false,
                newContent: null
            },
            {
                name: 'actions',
                title: 'Actions taken',
                isAdding: false,
                newContent: null
            }
        ]
    },
    methods: {
        fetchData() {
            axios.get('/api/cards').then(response => {
                this.cards = response.data;

                this.cards.forEach((card) => {
                    Vue.set(card, 'isEditing', false);
                });
            });
        },
        deleteCard(card) {
            axios.delete(`/api/cards/${card.id}`).then(response => {
                this.cards.splice(this.cards.indexOf(card), 1);
            });
        },
        editCard(card) {
            card.isEditing = true;
        },
        saveCard(card) {
            axios.post(`/api/cards/${card.id}`, {
                content: card.content,
                category: card.category
            }).then(response => {
                card.isEditing = false;
            });
        },
        cancelEditing(card) {
            card.isEditing = false;
        },
        addCard(category) {
            category.isAdding = true;
        },
        saveNewCard(category) {
            axios.put('/api/cards', {
                content: category.newContent,
                category: category.name
            }).then(response => {
                let newCard = response.data;
                newCard.isEditing = false;
                category.isAdding = false;
                category.newContent = null;
                this.cards.push(newCard);
            });
        },
        cancelAddCard(category) {
            category.isAdding = false;
        },
        filteredCards(filter) {
            return this.cards.filter(card => {
                return card.category === filter;
            })
        },
    },
    computed: {
    }
});