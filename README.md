# Sprint Retrospective Board

This is a demo project containing a simple front end connecting to an API which allows a user to create, update and 
delete notes belonging to one of three categories. 

It's using the newly-released Symfony 4 with flex for the API. The front end uses externally-linked VueJS and Bulma. No
NPM or Webpack at this stage.

## Running it yourself

Pull a clone from GitHub: `git clone https://github.com/damianpeterson/retrospective.git .`. Change directory to the 
newly-created `retropective` directory. Run `composer install` and edit the `.env` file to point to a database of your
choice. 

Create the database by running `bin/console doctrine:database:create` and migrate the tables with 
`bin/console doctrine:migrations:migrate`.

## Running tests

The spec tests can be run with `vendor/bin/phpspec run`

## Exploring the API

Copy and paste the contents of `swagger.yaml` into the [Swagger online editor](https://editor.swagger.io/) to explore 
the API interactively.

## CLI

There are three console methods available for interacting with the API via the CLI. Run `bin/console | grep app:card` to
see the methods available.