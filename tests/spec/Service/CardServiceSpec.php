<?php

namespace spec\App\Service;

use App\Entity\Card;
use App\Repository\CardRepository;
use Doctrine\ORM\EntityManager;
use App\Service\CardService;
use Doctrine\ORM\EntityManagerInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * Class CardServiceSpec
 * @package spec\App\Service
 * @mixin CardService
 */
class CardServiceSpec extends ObjectBehavior
{
    function let(EntityManager $entityManager)
    {
        $this->beConstructedWith($entityManager);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CardService::class);
    }

    function it_can_get_all_cards(
        EntityManager $entityManager,
        CardRepository $cardRepository,
        Card $card1,
        Card $card2
    )
    {
        $card1->getId()->willReturn(1);
        $card1->getCategory()->willReturn(Card::CATEGORY_BAD);
        $card1->getContent()->willReturn('one');
        $card1->getCreatedAt()->willReturn(new \DateTime('-1 day'));
        $card1->toArray()->shouldBeCalled();

        $card2->getId()->willReturn(2);
        $card2->getCategory()->willReturn(Card::CATEGORY_GOOD);
        $card2->getContent()->willReturn('two');
        $card2->getCreatedAt()->willReturn(new \DateTime('-1 hour'));
        $card2->toArray()->shouldBeCalled();

        $entityManager->getRepository(Card::class)->willReturn($cardRepository);
        $cardRepository->findCards(0, 100)->willReturn([$card1, $card2]);

        $cards = $this->getCards();

        $cards->shouldBeArray();
        $cards->shouldHaveCount(2);
    }

    function it_creates_a_new_card()
    {
        $newCard = $this->createCard('one', Card::CATEGORY_GOOD);
        $newCard->shouldBeArray();
        $newCard->shouldHaveKey('id');
        $newCard->shouldHaveKeyWithValue('content', 'one');
        $newCard->shouldHaveKeyWithValue('category', Card::CATEGORY_GOOD);
        $newCard->shouldHaveKey('created_at');
    }

    function it_fails_to_create_a_new_card_with_wrong_category()
    {
        $this->shouldThrow(
            new \Exception('Invalid category specified')
        )->duringCreateCard('this is the content', 'wrong');
    }

    function it_fails_to_create_card_with_too_much_content()
    {
        $longString = str_pad('x', 260, 'x');

        $this->shouldThrow(
            new \Exception('Content needs to not be empty and less than 256 characters')
        )->duringCreateCard($longString, Card::CATEGORY_GOOD);
    }

    function it_fails_to_create_a_card_with_no_content()
    {
        $this->shouldThrow(
            new \Exception('Content needs to not be empty and less than 256 characters')
        )->duringCreateCard('', Card::CATEGORY_GOOD);
    }

    function it_can_update_a_card(
        EntityManager $entityManager,
        CardRepository $cardRepository,
        Card $card
    )
    {
        $entityManager->getRepository(Card::class)->willReturn($cardRepository);
        $cardRepository->find(1)->willReturn($card);

        $card->setContent('one altered')->shouldBeCalled();
        $card->setCategory(Card::CATEGORY_BAD)->shouldBeCalled();
        $card->setUpdatedAt(Argument::any())->shouldBeCalled();
        $card->toArray()->shouldBeCalled();
        $card->getDeletedAt()->shouldBeCalled();
        $entityManager->persist($card)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();

        $updatedCard = $this->updateCard(1, 'one altered', Card::CATEGORY_BAD);
        $updatedCard->shouldBeArray();
    }

    function it_fails_to_update_a_card_with_invalid_id(
        EntityManager $entityManager,
        CardRepository $cardRepository
    )
    {
        $entityManager->getRepository(Card::class)->willReturn($cardRepository);
        $cardRepository->find(1)->willReturn(null);

        $this->shouldThrow(new \Exception('Card not found'))->duringUpdateCard(1, 'Anything', Card::CATEGORY_BAD);
    }

    function it_can_delete_a_card(
        EntityManager $entityManager,
        CardRepository $cardRepository,
        Card $card
    )
    {
        $entityManager->getRepository(Card::class)->willReturn($cardRepository);
        $cardRepository->find(1)->willReturn($card);

        $card->setDeletedAt(Argument::type(\DateTime::class))->shouldBeCalled();
        $card->toArray()->shouldBeCalled();
        $card->getDeletedAt()->shouldBeCalled();
        $entityManager->persist($card)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        $this->deleteCard(1);
    }

    function it_fails_to_delete_a_card_that_is_already_deleted(
        EntityManager $entityManager,
        CardRepository $cardRepository,
        Card $card
    )
    {
        $entityManager->getRepository(Card::class)->willReturn($cardRepository);
        $cardRepository->find(1)->willReturn(null);

        $this->shouldThrow(new \Exception('Card not found'))->duringDeleteCard(1);
    }
}
