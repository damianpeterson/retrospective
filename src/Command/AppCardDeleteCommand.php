<?php

namespace App\Command;

use App\Service\CardService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppCardDeleteCommand extends Command
{
    protected static $defaultName = 'app:card:delete';
    private $cardService;

    public function __construct(string $name = null, CardService $cardService)
    {
        parent::__construct($name);
        $this->cardService = $cardService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Delete a card')
            ->addArgument('id', InputArgument::REQUIRED, 'The ID of the card to delete')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $id = $input->getArgument('id');

        try {
            $card = $this->cardService->deleteCard($id);
            $io->success('Card successfully deleted');
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
    }
}
