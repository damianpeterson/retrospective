<?php

namespace App\Command;

use App\Service\CardService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppCardUpdateCommand extends Command
{
    protected static $defaultName = 'app:card:update';
    private $cardService;

    public function __construct(string $name = null, CardService $cardService)
    {
        parent::__construct($name);
        $this->cardService = $cardService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Update a card')
            ->addArgument('id', InputArgument::REQUIRED, 'The ID of the card to update')
            ->addArgument('content', InputArgument::REQUIRED, 'The content of the card')
            ->addArgument('category', InputArgument::REQUIRED, 'The category of the card')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $id = $input->getArgument('id');
        $content = $input->getArgument('content');
        $category = $input->getArgument('category');

        try {
            $this->cardService->updateCard($id, $content, $category);
            $io->success('Card successfully updated');
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }
    }
}
