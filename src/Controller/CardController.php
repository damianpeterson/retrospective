<?php

namespace App\Controller;

use App\Service\CardService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package App\Controller
 * @Route("/api")
 */
class CardController extends Controller
{
    /**
     * @Route(
     *     "/cards",
     *     name="get_cards"
     * )
     * @Method({"GET"})
     */
    public function getCards(CardService $cardService, Request $request)
    {
        $cards = $cardService->getCards($request->query->get('offset', 0), $request->query->get('limit', 100));
        return new JsonCorsResponse($cards, Response::HTTP_OK);
    }

    /**
     * @Route(
     *     "/cards",
     *     name="create_card"
     * )
     * @Method({"PUT", "OPTIONS"})
     * @throws \Exception
     */
    public function createCard(CardService $cardService, Request $request)
    {
        $json = json_decode($request->getContent());

        if (empty($json->content) === true || empty($json->category) === true) {
            return new JsonCorsResponse('Content and category are required', Response::HTTP_BAD_REQUEST);
        }

        try {
            $card = $cardService->createCard($json->content, $json->category);
            return new JsonCorsResponse($card, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return new JsonCorsResponse('Unable to create card', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route(
     *     "/cards/{id}",
     *     name="update_card",
     *     requirements={"id"="\d+"}
     * )
     * @Method({"POST", "OPTIONS"})
     * @throws \Exception
     */
    public function updateCard(CardService $cardService, Request $request, int $id)
    {
        $json = json_decode($request->getContent());

        if (empty($json->content) === true || empty($json->category) === true) {
            return new JsonCorsResponse('Content and category are required', Response::HTTP_BAD_REQUEST);
        }

        try {
            $card = $cardService->updateCard($id, $json->content, $json->category);
            return new JsonCorsResponse($card, Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonCorsResponse('Unable to update card', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route(
     *     "/cards/{id}",
     *     name="delete_card",
     *     requirements={"id"="\d+"}
     * )
     * @Method({"DELETE"})
     * @throws \Exception
     */
    public function deleteCard(CardService $cardService, Request $request, int $id)
    {
        try {
            $card = $cardService->deleteCard($id);
            return new JsonCorsResponse($card, Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonCorsResponse('Unable to update card', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
