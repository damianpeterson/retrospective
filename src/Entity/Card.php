<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardRepository")
 */
class Card
{
    const CATEGORY_GOOD = 'good';
    const CATEGORY_BAD = 'bad';
    const CATEGORY_ACTIONS = 'actions';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="content", type="string", length=255, nullable=false)
     * @var string The content of the card. Can be up to 255 characters long.
     */
    private $content;

    /**
     * @ORM\Column(name="category", type="string", length=255, nullable=false)
     * @var string The category this card belongs to. Restricted to 'good', 'bad' or 'actions'
     */
    private $category;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \DateTime $deletedAt
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'content' => $this->getContent(),
            'category' => $this->getCategory(),
            'created_at' => $this->getCreatedAt()->format(DATE_ISO8601)
        ];
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Card
     * @throws \Exception
     */
    public function setContent($content)
    {
        if (strlen($content) > 255 || strlen($content) === 0 || empty($content)) {
            throw new \Exception('Content needs to not be empty and less than 256 characters');
        }
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return Card
     * @throws \Exception
     */
    public function setCategory($category)
    {
        $validCategories = [
            self::CATEGORY_GOOD,
            self::CATEGORY_BAD,
            self::CATEGORY_ACTIONS
        ];

        if (in_array($category, $validCategories) === false) {
            throw new \Exception('Invalid category specified');
        }

        $this->category = $category;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Card
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Card
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return Card
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }
}
