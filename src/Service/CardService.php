<?php

namespace App\Service;

use App\Entity\Card;
use Doctrine\ORM\EntityManagerInterface;

class CardService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get all cards in a range
     *
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getCards(int $offset = 0, int $limit = 100): array
    {
        $cardRepository = $this->entityManager->getRepository(Card::class);
        /** @var Card[] $cards */
        $cards = $cardRepository->findCards($offset, $limit);

        $cardsArray = [];

        foreach ($cards as $card) {
            $cardsArray[] = $card->toArray();
        }

        return $cardsArray;
    }

    /**
     * Creates a new card
     *
     * @param string $content
     * @param string $category
     * @return array
     * @throws \Exception
     */
    public function createCard(string $content, string $category): array
    {
        $card = new Card();

        try {
            $card->setContent($content);
            $card->setCategory($category);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        $card->setCreatedAt(new \DateTime('now'));
        $this->entityManager->persist($card);
        $this->entityManager->flush();

        return $card->toArray();
    }

    /**
     * Updates a card
     *
     * @param int $id
     * @param string $content
     * @param string $category
     * @return mixed
     * @throws \Exception
     */
    public function updateCard(int $id, string $content, string $category): array
    {
        $cardRepository = $this->entityManager->getRepository(Card::class);
        $card = $cardRepository->find($id);

        if (empty($card) || $card->getDeletedAt() !== null) {
            throw new \Exception('Card not found');
        }

        $card->setContent($content);
        $card->setCategory($category);
        $card->setUpdatedAt(new \DateTime('now'));
        $this->entityManager->persist($card);
        $this->entityManager->flush();

        return $card->toArray();
    }

    /**
     * Delete a card
     *
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function deleteCard(int $id): array
    {
        $cardRepository = $this->entityManager->getRepository(Card::class);
        $card = $cardRepository->find($id);

        if (empty($card) || $card->getDeletedAt() !== null) {
            throw new \Exception('Card not found');
        }

        $card->setDeletedAt(new \DateTime('now'));

        $this->entityManager->persist($card);
        $this->entityManager->flush();

        return $card->toArray();
    }
}
