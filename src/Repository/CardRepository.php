<?php

namespace App\Repository;

use App\Entity\Card;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CardRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Card::class);
    }

    public function findCards(int $offset = 0, int $limit = 100)
    {
        $queryBuilder = $this->createQueryBuilder('cards');

        $queryBuilder
            ->where('cards.deletedAt IS NULL')
            ->orderBy('cards.createdAt', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
